﻿using DataProject;
using DataProject.Repositories;
using ServicesProject;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var dbContext = new AppDBContext();
            {
                var NewsRepository = new NewsRepository(dbContext);
                var RssRepository = new RssRepository(dbContext);

                IGetAndRecordNewsService fillNewsService = new GetAndRecordNewsService(NewsRepository, RssRepository);

                foreach (var s in fillNewsService.DownloadAndRecordNews())
                {
                    Console.WriteLine(s);
                }

                Console.ReadKey();
            }
        }
    }
}
