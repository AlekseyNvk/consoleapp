﻿using DataProject.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesProject
{
    public interface IGetAndRecordNewsService
    {
        IEnumerable<Tuple<int, string>> DownloadAllRss(int maxStreamsCount = 4);
        IEnumerable<string> DownloadAndRecordNews();
        Task<Tuple<int, string>> DownloadOneRssAsync(RSS rss);
    }
}
