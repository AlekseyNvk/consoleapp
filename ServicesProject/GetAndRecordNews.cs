﻿using DataProject.Entity;
using DataProject.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ServicesProject
{
    public class GetAndRecordNewsService : IGetAndRecordNewsService
    {
        protected readonly INewsRepository _newsRepository;
        protected readonly IRssRepository _rssRepository;

        public GetAndRecordNewsService(INewsRepository newsRepository, IRssRepository rssRepository)
        {
            this._newsRepository = newsRepository;
            this._rssRepository = rssRepository;
        }

        public async Task<Tuple<int, string>> DownloadOneRssAsync(RSS rss)
        {
            using (var webClient = new WebClient() { Encoding = Encoding.UTF8 })
            {
                return Tuple.Create(rss.Id, (await webClient.DownloadStringTaskAsync(rss.Url)).Trim());
            }
        }

        public IEnumerable<Tuple<int, string>> DownloadAllRss(int maxStreamsCount = 4)
        {
            var rsses = _rssRepository.GetRSSes();

            var downloadTasks = new List<Task<Tuple<int, string>>>();
            var unCompletedTasks = new List<Task<Tuple<int, string>>>();

            foreach (var r in rsses)
            {
                unCompletedTasks = downloadTasks.Where(x => !x.IsCompleted).ToList();

                if (unCompletedTasks.Count() >= maxStreamsCount)
                {
                    Task.WaitAny(unCompletedTasks.ToArray());
                }

                downloadTasks.Add(this.DownloadOneRssAsync(r));
            }

            Task.WaitAll(downloadTasks.ToArray());
            return downloadTasks.Select(x => x.Result);
        }

        protected IEnumerable<string> parseAndRecordNews(IEnumerable<Tuple<int, string>> newsString)
        {
            var dateFormats = new[] {
                        new CultureInfo("en-US").DateTimeFormat.RFC1123Pattern, // for habr
                        "ddd, dd MMM yyyy HH:mm:ss zzz", // for interfax
                    };

            var LogStrings = new List<string>();

            foreach (var n in newsString)
            {
                var items = XDocument.Parse(n.Item2).Root.Element("channel").Elements("item");
                int countUnRecordNews = 0;
                foreach (var i in items)
                {
                    var title = i.Element("title").Value;
                    var date = DateTime.ParseExact(i.Element("pubDate").Value, dateFormats, CultureInfo.InvariantCulture, DateTimeStyles.None);

                    if (_newsRepository.isExisting(title, date))
                    {
                        ++countUnRecordNews; continue;
                    }

                    var news = new News()
                    {
                        Title = title,
                        PublishDate = date,
                        Description = i.Element("description").Value,
                        Url = i.Element("link").Value,
                        RssId = n.Item1,
                    };

                    _newsRepository.CreateNews(news);
                }

                LogStrings.Add($"источник номер: {n.Item1}; получено новостей: {items.Count()}; сохранено новостей: {items.Count() - countUnRecordNews}");
            }

            return LogStrings;
        }

        public IEnumerable<string> DownloadAndRecordNews()
        {
            return this.parseAndRecordNews(this.DownloadAllRss());
        }
    }
}
