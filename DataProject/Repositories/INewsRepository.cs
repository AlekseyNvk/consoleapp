﻿using DataProject.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProject.Repositories
{
    public interface INewsRepository
    {
        News CreateNews(News news);
        void DeleteNews(int Id);
        IEnumerable<News> GetNews(RSS rss = null, int skip = 0, int take = int.MaxValue);
        bool isExisting(string title, DateTime date);
        void UpdateNews(News news);
    }
}
