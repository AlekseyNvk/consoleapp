﻿using DataProject.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProject.Repositories
{
    public sealed class NewsRepository: INewsRepository
    {
        private readonly AppDBContext _dbContext;

        public NewsRepository(AppDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool isExisting(string title, DateTime date)
        {
            return _dbContext.News.Any(n => n.Title == title && n.PublishDate == date);
        }

        public IEnumerable<News> GetNews(RSS rss = null, int skip = 0, int take = int.MaxValue)
        {
            if (rss is null)
            {
                return _dbContext.News.Skip(skip).Take(take).ToList();
            }

            return _dbContext.News.Where(n => n.RssId == rss.Id).Skip(skip).Take(take).ToList();
        }

        public News CreateNews(News news)
        {
            _dbContext.News.Add(news);
            _dbContext.SaveChanges();

            return news;
        }

        public void UpdateNews(News news)
        {
            _dbContext.Entry(news).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public void DeleteNews(int Id)
        {
            var n = _dbContext.News.Find(Id);
            _dbContext.News.Remove(n);
            _dbContext.SaveChanges();
        }
    }
}
