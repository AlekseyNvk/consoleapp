﻿using DataProject.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProject.Repositories
{
    public sealed class RssRepository: IRssRepository
    {
        private readonly AppDBContext _dbContext;

        public RssRepository(AppDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<RSS> GetRSSes()
        {
            return _dbContext.Rss.ToList();
        }
    }
}
