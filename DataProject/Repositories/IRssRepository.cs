﻿using DataProject.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProject.Repositories
{
    public interface IRssRepository
    {
        IEnumerable<RSS> GetRSSes();
    }
}
