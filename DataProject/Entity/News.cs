﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProject.Entity
{
    public class News
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        public DateTime PublishDate { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public RSS Rss { get; set; }

        public int RssId { get; set; }
    }
}
