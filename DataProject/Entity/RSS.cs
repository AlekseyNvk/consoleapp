﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProject.Entity
{
    public class RSS
    {
        [Key]
        public int Id { get; set; }

        public string Url { get; set; }

        public string Name { get; set; }

        public ICollection<News> News { get; set; }
    }
}
