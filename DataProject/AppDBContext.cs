﻿using DataProject.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProject
{
    public class AppDBContext : System.Data.Entity.DbContext
    {
        public AppDBContext() : base("LConnection")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<AppDBContext>(new BdInitializer());
        }

        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<RSS> Rss { get; set; }
    }
}
