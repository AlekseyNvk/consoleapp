﻿using DataProject.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProject
{
    public class BdInitializer : DropCreateDatabaseIfModelChanges<AppDBContext>
    {
        protected override void Seed(AppDBContext dbContext)
        {
            var habr = new RSS()
            {
                Url = @"https://habr.com/rss/interesting/",
                Name = "Habr",
            };

            var interfax = new RSS()
            {
                Url = @"https://www.interfax.by/news/feed",
                Name = "Interfax",
            };

            dbContext.Rss.Add(habr);
            dbContext.Rss.Add(interfax);
            dbContext.SaveChanges();
        }
    }
}
